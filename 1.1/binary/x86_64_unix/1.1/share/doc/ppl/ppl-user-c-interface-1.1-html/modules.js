var modules =
[
    [ "C Language Interface", "group__PPL__C__interface.html", null ],
    [ "Library Initialization and Finalization", "group__Init.html", "group__Init" ],
    [ "Version Checking", "group__Version.html", "group__Version" ],
    [ "Error Handling", "group__Error.html", "group__Error" ],
    [ "Timeout Handling", "group__Timeout.html", "group__Timeout" ],
    [ "Library Datatypes", "group__Datatypes.html", "group__Datatypes" ],
    [ "C++ Language Interface", "../ppl-user-1.1-html/group__PPL__CXX__interface.html", "group__PPL__CXX__interface" ]
];