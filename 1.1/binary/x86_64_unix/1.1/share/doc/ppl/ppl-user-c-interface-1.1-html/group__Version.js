var group__Version =
[
    [ "PPL_VERSION", "group__Version.html#ga79bd3696a302bb721ef8168fa5d650fb", null ],
    [ "PPL_VERSION_MAJOR", "group__Version.html#gae19dfe022c51c874d905e2a7c81c18f1", null ],
    [ "PPL_VERSION_MINOR", "group__Version.html#ga8458e58a5e857de11c35ce3076a70ab8", null ],
    [ "PPL_VERSION_REVISION", "group__Version.html#gabadfeba0a415d210cdd6d7309365e800", null ],
    [ "PPL_VERSION_BETA", "group__Version.html#ga7a529f51bfebdd4b3e69c866dced9bc1", null ],
    [ "ppl_version_major", "group__Version.html#gabdf8f4e9416010573d48e21cf0584762", null ],
    [ "ppl_version_minor", "group__Version.html#gab51f10ea37de5299e9bc840afa206d4e", null ],
    [ "ppl_version_revision", "group__Version.html#gaa50f472bf6d1869a2fc1500d2fdf8213", null ],
    [ "ppl_version_beta", "group__Version.html#ga676f27ad949159a3b79577f78870f60f", null ],
    [ "ppl_version", "group__Version.html#gac6abee034f6ac000f15d1206fdeb8316", null ],
    [ "ppl_banner", "group__Version.html#ga63fef7041da34ac5bb372a2535d99377", null ]
];