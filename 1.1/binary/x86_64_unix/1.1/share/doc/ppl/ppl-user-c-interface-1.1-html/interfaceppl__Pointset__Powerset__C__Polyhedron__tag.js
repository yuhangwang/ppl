var interfaceppl__Pointset__Powerset__C__Polyhedron__tag =
[
    [ "ppl_Pointset_Powerset_C_Polyhedron_omega_reduce", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#a26a124537b8995832b2cb157abc7cf07", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_size", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#a0bf696420bbdc5c2ef0993f61f2273b7", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_geometrically_covers_Pointset_Powerset_C_Polyhedron", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#aeb8456ecf06307c108046a8c665154c9", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_geometrically_equals_Pointset_Powerset_C_Polyhedron", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#a024a52502b35029ff2ba102323d8ae5b", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_add_disjunct", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#a91f0ccf4325f53430d7347ca73182f52", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_drop_disjunct", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#aae5f47efa7c023e45aed9be161dce99f", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_drop_disjuncts", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#a1b2ee4d834f0651a41647ac5b5e0f912", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_pairwise_reduce", "interfaceppl__Pointset__Powerset__C__Polyhedron__tag.html#ab595ea1166752d23aba600af43417bd8", null ]
];