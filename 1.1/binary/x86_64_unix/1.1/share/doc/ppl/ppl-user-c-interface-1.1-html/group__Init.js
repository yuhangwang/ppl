var group__Init =
[
    [ "ppl_initialize", "group__Init.html#ga7c98bac1ccfac398657571409a3fcd9c", null ],
    [ "ppl_finalize", "group__Init.html#ga4af2931282f6a5f1b3444fb8e833d6a1", null ],
    [ "ppl_set_rounding_for_PPL", "group__Init.html#ga635128363efd9f4c8c63e21702f40183", null ],
    [ "ppl_restore_pre_PPL_rounding", "group__Init.html#gafdb2c887a3a2d1daf54a764d0d7f7f76", null ],
    [ "ppl_irrational_precision", "group__Init.html#gaf090a4178ad2e0dc0ea185961e13b3f1", null ],
    [ "ppl_set_irrational_precision", "group__Init.html#ga234d52733263ece8ac36c980a80292c9", null ]
];