var group__Error =
[
    [ "ppl_enum_error_code", "group__Error.html#ga0c0ab09a97e49f85f42c966e14cfdee6", [
      [ "PPL_ERROR_OUT_OF_MEMORY", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6af29a62bdfcf44dbeec14afb409035438", null ],
      [ "PPL_ERROR_INVALID_ARGUMENT", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a093ae02f8ceebbc40321d1eaf186f726", null ],
      [ "PPL_ERROR_DOMAIN_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a8c65fedb9893ebb49394a3cff2f19a49", null ],
      [ "PPL_ERROR_LENGTH_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a6aa28e72200f879cbb83a36b60644fb2", null ],
      [ "PPL_ARITHMETIC_OVERFLOW", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6abd22c4eaabb49bcb6c654a25e402c64a", null ],
      [ "PPL_STDIO_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a0860beb1b1c54c9f774b532d92404992", null ],
      [ "PPL_ERROR_INTERNAL_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a7dae659f72f654afae1c5c6f331192a3", null ],
      [ "PPL_ERROR_UNKNOWN_STANDARD_EXCEPTION", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a03b444004b1031ddb1a51289b9d83106", null ],
      [ "PPL_ERROR_UNEXPECTED_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6ab42a97917f263e8305d561f52cbd9c57", null ],
      [ "PPL_TIMEOUT_EXCEPTION", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a24fd14db2a8ae42b3abcae3288e46edf", null ],
      [ "PPL_ERROR_LOGIC_ERROR", "group__Error.html#gga0c0ab09a97e49f85f42c966e14cfdee6a8bf236a519f7da9efd5f874a17d9689d", null ]
    ] ],
    [ "ppl_set_error_handler", "group__Error.html#gad6765993c08a2ae2f0ef377f822f4d33", null ]
];