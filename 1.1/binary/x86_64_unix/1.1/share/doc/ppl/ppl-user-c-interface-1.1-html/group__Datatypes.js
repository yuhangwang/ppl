var group__Datatypes =
[
    [ "ppl_dimension_type", "group__Datatypes.html#ga32ce20a24d131c3ec8274b907176bfec", null ],
    [ "ppl_io_variable_output_function_type", "group__Datatypes.html#ga1822baf29ede5879f1c6ed77f0ca5984", null ],
    [ "ppl_Coefficient_t", "group__Datatypes.html#gae50df8ccfc72a20b58ab7854ad2ccba9", null ],
    [ "ppl_const_Coefficient_t", "group__Datatypes.html#gaf1c4903edb1a626a36a7220038f963c4", null ],
    [ "ppl_Linear_Expression_t", "group__Datatypes.html#gadc58d1f680927df3786f41bdb47fa26b", null ],
    [ "ppl_const_Linear_Expression_t", "group__Datatypes.html#ga25f4b655851f0f561cdddf94f667e833", null ],
    [ "ppl_Constraint_t", "group__Datatypes.html#gaedee0800f6e695c36ace737095ccef5b", null ],
    [ "ppl_const_Constraint_t", "group__Datatypes.html#ga4384ab8e17fe9415f5146ece650cf9c3", null ],
    [ "ppl_Constraint_System_t", "group__Datatypes.html#ga84f27605574583d674403f6d71a73a24", null ],
    [ "ppl_const_Constraint_System_t", "group__Datatypes.html#gad55dee4843ce4694afab11a60f16ef40", null ],
    [ "ppl_Constraint_System_const_iterator_t", "group__Datatypes.html#ga5a7338130cf6d33d28dd40ed560a24d6", null ],
    [ "ppl_const_Constraint_System_const_iterator_t", "group__Datatypes.html#ga88e874dbd26b94133467d81b006f0c3f", null ],
    [ "ppl_Generator_t", "group__Datatypes.html#gabf20fcbeb06790546a112b72eb0811f8", null ],
    [ "ppl_const_Generator_t", "group__Datatypes.html#ga429b479c97f57c41763a019c2dc5bf10", null ],
    [ "ppl_Generator_System_t", "group__Datatypes.html#ga747bcaa4d01a8065758096b1ff0eb551", null ],
    [ "ppl_const_Generator_System_t", "group__Datatypes.html#ga6069aff3ad06a502f746df4adec1c16c", null ],
    [ "ppl_Generator_System_const_iterator_t", "group__Datatypes.html#gac803b1dab9dd0072acb6fdda204efd26", null ],
    [ "ppl_const_Generator_System_const_iterator_t", "group__Datatypes.html#gad8582d9f10213d638e87de8f0aa91a01", null ],
    [ "ppl_Congruence_t", "group__Datatypes.html#ga64dda1bda497aab131fc204ed9e55894", null ],
    [ "ppl_const_Congruence_t", "group__Datatypes.html#ga3b2653e715f41511257a84f35be7fba9", null ],
    [ "ppl_Congruence_System_t", "group__Datatypes.html#ga5e59406ab43e2f1851ea2e066137b4bd", null ],
    [ "ppl_const_Congruence_System_t", "group__Datatypes.html#ga4aa820c3f87b201ce364e854aa37dba5", null ],
    [ "ppl_Congruence_System_const_iterator_t", "group__Datatypes.html#ga5e128d772c07f54f135cac19e9a6ffbe", null ],
    [ "ppl_const_Congruence_System_const_iterator_t", "group__Datatypes.html#gae8610cd48952cf1dc71b525235f6f583", null ],
    [ "ppl_Grid_Generator_t", "group__Datatypes.html#gaefc9900fe07bfdee6310e4705ea0bfec", null ],
    [ "ppl_const_Grid_Generator_t", "group__Datatypes.html#gaf78df97484e374abe5d4e0f8973a622d", null ],
    [ "ppl_Grid_Generator_System_t", "group__Datatypes.html#gac0d389ac714f0d4d3e6950fd692e7443", null ],
    [ "ppl_const_Grid_Generator_System_t", "group__Datatypes.html#gab49861123b31a74dddcdffb4b8fd110a", null ],
    [ "ppl_Grid_Generator_System_const_iterator_t", "group__Datatypes.html#ga56e2574a7d2f006333151867599e8fa3", null ],
    [ "ppl_const_Grid_Generator_System_const_iterator_t", "group__Datatypes.html#gaf02dc1cffb4d3d79720ebafbe872be03", null ],
    [ "ppl_MIP_Problem_t", "group__Datatypes.html#ga12c368fa0bdd20b907d9ab8e92e999d2", null ],
    [ "ppl_const_MIP_Problem_t", "group__Datatypes.html#gaec4f5c405067bcd0483d84591bc7c48e", null ],
    [ "ppl_PIP_Problem_t", "group__Datatypes.html#ga73918c511d88c3fd8ad4de602c4d20df", null ],
    [ "ppl_const_PIP_Problem_t", "group__Datatypes.html#gac9397590f57ede293eed34366aec11dd", null ],
    [ "ppl_PIP_Tree_Node_t", "group__Datatypes.html#gaf4f8f4bacd6f148fbbfb9209fb5b2b33", null ],
    [ "ppl_const_PIP_Tree_Node_t", "group__Datatypes.html#gaceb51fb97c11d86059188525d87ceaaf", null ],
    [ "ppl_PIP_Decision_Node_t", "group__Datatypes.html#ga25f9463d3385bd998e4fabcdce8dec5a", null ],
    [ "ppl_const_PIP_Decision_Node_t", "group__Datatypes.html#ga43246761a1030d751e756845a246bf68", null ],
    [ "ppl_PIP_Solution_Node_t", "group__Datatypes.html#ga68fae2777effe5d0ed0e3dcec1310e1f", null ],
    [ "ppl_const_PIP_Solution_Node_t", "group__Datatypes.html#ga934e8eb1ed3d9f688ffafe0f82ad02f6", null ],
    [ "ppl_Artificial_Parameter_t", "group__Datatypes.html#gac3cf4d5c81ff784729bb5d1c8d48ce23", null ],
    [ "ppl_const_Artificial_Parameter_t", "group__Datatypes.html#ga4af3a60e2d8ec27d56cf4a555cc1748e", null ],
    [ "ppl_Artificial_Parameter_Sequence_t", "group__Datatypes.html#ga9448cd0d782a819c066e27cf3ebc1add", null ],
    [ "ppl_const_Artificial_Parameter_Sequence_t", "group__Datatypes.html#ga261d916137f01c0effdefeb266a73172", null ],
    [ "ppl_Artificial_Parameter_Sequence_const_iterator_t", "group__Datatypes.html#ga2fdde49c81e0fd5d54dba2cb056847b3", null ],
    [ "ppl_const_Artificial_Parameter_Sequence_const_iterator_t", "group__Datatypes.html#ga7364448b56d85cb45c512ca35678952a", null ],
    [ "ppl_Polyhedron_t", "group__Datatypes.html#ga97b246f11809448c53f089779c0019a1", null ],
    [ "ppl_const_Polyhedron_t", "group__Datatypes.html#gabc52e1474c4b78458b4c13ddbfdc8e56", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_t", "group__Datatypes.html#gadb1edf876e8c62fb7d2630a9280224f1", null ],
    [ "ppl_const_Pointset_Powerset_C_Polyhedron_t", "group__Datatypes.html#ga82657533544652eb3ec1334a72da137e", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_iterator_t", "group__Datatypes.html#ga2f2a1aa8fbda2e53218cecba593f7427", null ],
    [ "ppl_const_Pointset_Powerset_C_Polyhedron_iterator_t", "group__Datatypes.html#ga6d1de7e82c3d57a0a6229187078537d7", null ],
    [ "ppl_Pointset_Powerset_C_Polyhedron_const_iterator_t", "group__Datatypes.html#ga576b0ae433a83ef58a3c174a86623582", null ],
    [ "ppl_const_Pointset_Powerset_C_Polyhedron_const_iterator_t", "group__Datatypes.html#gaf2faa73b338e8563f6c022739e5ecc6f", null ],
    [ "ppl_enum_Constraint_Type", "group__Datatypes.html#gae5679e4f41db32b7b91dfc81574fc1d6", [
      [ "PPL_CONSTRAINT_TYPE_LESS_THAN", "group__Datatypes.html#ggae5679e4f41db32b7b91dfc81574fc1d6a81067f9cf8984d1b3febca4488a137e1", null ],
      [ "PPL_CONSTRAINT_TYPE_LESS_OR_EQUAL", "group__Datatypes.html#ggae5679e4f41db32b7b91dfc81574fc1d6af8c3e04d66cd611db5efcd92a6c52425", null ],
      [ "PPL_CONSTRAINT_TYPE_EQUAL", "group__Datatypes.html#ggae5679e4f41db32b7b91dfc81574fc1d6aa166787e251ef2cbaad9213d8e9d2653", null ],
      [ "PPL_CONSTRAINT_TYPE_GREATER_OR_EQUAL", "group__Datatypes.html#ggae5679e4f41db32b7b91dfc81574fc1d6a9e4eb4979711bf6699ec8e0f335de476", null ],
      [ "PPL_CONSTRAINT_TYPE_GREATER_THAN", "group__Datatypes.html#ggae5679e4f41db32b7b91dfc81574fc1d6a9029c33b6876c560bf78b5b86d0367c0", null ]
    ] ],
    [ "ppl_enum_Generator_Type", "group__Datatypes.html#gaf449476be1ec42bab45fb86e7478bdf9", [
      [ "PPL_GENERATOR_TYPE_LINE", "group__Datatypes.html#ggaf449476be1ec42bab45fb86e7478bdf9a132c76cbee67b5bc2ac9e09af9c68c4f", null ],
      [ "PPL_GENERATOR_TYPE_RAY", "group__Datatypes.html#ggaf449476be1ec42bab45fb86e7478bdf9a1304ca7fa8d3e734d1677709a37d5d28", null ],
      [ "PPL_GENERATOR_TYPE_POINT", "group__Datatypes.html#ggaf449476be1ec42bab45fb86e7478bdf9afdbee5cd06ff135cbf9614b661c9e923", null ],
      [ "PPL_GENERATOR_TYPE_CLOSURE_POINT", "group__Datatypes.html#ggaf449476be1ec42bab45fb86e7478bdf9ab32d9464a7101eb1893562646a3cdd1a", null ]
    ] ],
    [ "ppl_enum_Grid_Generator_Type", "group__Datatypes.html#ga5b805182b79b6fb873d0624f7dfd2ee1", [
      [ "PPL_GRID_GENERATOR_TYPE_LINE", "group__Datatypes.html#gga5b805182b79b6fb873d0624f7dfd2ee1a59042baec996bc386154a1825ad56de0", null ],
      [ "PPL_GRID_GENERATOR_TYPE_PARAMETER", "group__Datatypes.html#gga5b805182b79b6fb873d0624f7dfd2ee1aa05a8932097b094ca179e99e2d80b6ae", null ],
      [ "PPL_GRID_GENERATOR_TYPE_POINT", "group__Datatypes.html#gga5b805182b79b6fb873d0624f7dfd2ee1a3e5170687052e3962de866e2ab44d8f8", null ]
    ] ],
    [ "ppl_enum_Bounded_Integer_Type_Width", "group__Datatypes.html#ga4b10979f4464f0574331e442e9025d3c", [
      [ "PPL_BITS_8", "group__Datatypes.html#gga4b10979f4464f0574331e442e9025d3cae072091ed81a04a85178bba39494a601", null ],
      [ "PPL_BITS_16", "group__Datatypes.html#gga4b10979f4464f0574331e442e9025d3ca5c5c70a1d41e6e79718bbeb33df7cad4", null ],
      [ "PPL_BITS_32", "group__Datatypes.html#gga4b10979f4464f0574331e442e9025d3ca48a27a450babc28cd9e37b24e4c7f870", null ],
      [ "PPL_BITS_64", "group__Datatypes.html#gga4b10979f4464f0574331e442e9025d3ca8cbde0e6dec605c51ef708fb8cd2b52e", null ],
      [ "PPL_BITS_128", "group__Datatypes.html#gga4b10979f4464f0574331e442e9025d3ca5637cd5e0441762c2efba8ec4549156a", null ]
    ] ],
    [ "ppl_enum_Bounded_Integer_Type_Representation", "group__Datatypes.html#ga6b8429baa2a70746836586aad6905b64", [
      [ "PPL_UNSIGNED", "group__Datatypes.html#gga6b8429baa2a70746836586aad6905b64a8a337487ebb2a4938a04103368bc24dc", null ],
      [ "PPL_SIGNED_2_COMPLEMENT", "group__Datatypes.html#gga6b8429baa2a70746836586aad6905b64a81a7221b987cf71e9577f1d3162fc994", null ]
    ] ],
    [ "ppl_enum_Bounded_Integer_Type_Overflow", "group__Datatypes.html#ga4813a2ed52eb4aeac03bde07be0ddd83", [
      [ "PPL_OVERFLOW_WRAPS", "group__Datatypes.html#gga4813a2ed52eb4aeac03bde07be0ddd83a60e9236626a09f76d3fe049e03bd6926", null ],
      [ "PPL_OVERFLOW_UNDEFINED", "group__Datatypes.html#gga4813a2ed52eb4aeac03bde07be0ddd83ab602a0c001878a0debc775c4bee33777", null ],
      [ "PPL_OVERFLOW_IMPOSSIBLE", "group__Datatypes.html#gga4813a2ed52eb4aeac03bde07be0ddd83ab34d1f06ae438f58dd44c6262b3281ce", null ]
    ] ],
    [ "ppl_max_space_dimension", "group__Datatypes.html#gae8e2db00ec6575b865057ffd3195327b", null ],
    [ "ppl_not_a_dimension", "group__Datatypes.html#gadc14acd60d7235a72f88850b9a917eca", null ],
    [ "ppl_io_print_variable", "group__Datatypes.html#ga7593947fa164900140d09c08b56c6cd1", null ],
    [ "ppl_io_fprint_variable", "group__Datatypes.html#ga56026e685fb0fe336a5f346580f84ff0", null ],
    [ "ppl_io_asprint_variable", "group__Datatypes.html#ga13f1f84a46d70b91637cb9d6af54af6b", null ],
    [ "ppl_io_set_variable_output_function", "group__Datatypes.html#ga0764a0e607ec4de08a53e6d3b109a714", null ],
    [ "ppl_io_get_variable_output_function", "group__Datatypes.html#gac37b94ca7dc0c8bb9797e6d91e16492e", null ],
    [ "ppl_io_wrap_string", "group__Datatypes.html#ga763a5fa98155383055703dc08cb3e11a", null ],
    [ "PPL_COMPLEXITY_CLASS_POLYNOMIAL", "group__Datatypes.html#ga1fd59c6502c8cbb9dbb802e4af34c940", null ],
    [ "PPL_COMPLEXITY_CLASS_SIMPLEX", "group__Datatypes.html#gab9fc89c0e89d4487a1f69a26849b682a", null ],
    [ "PPL_COMPLEXITY_CLASS_ANY", "group__Datatypes.html#ga735949470e98393af90b12f534b06cba", null ],
    [ "PPL_POLY_CON_RELATION_IS_DISJOINT", "group__Datatypes.html#gae439fa617415f2153f4793041c41bd9f", null ],
    [ "PPL_POLY_CON_RELATION_STRICTLY_INTERSECTS", "group__Datatypes.html#gaef15021f65d2810602a9a1bd06fc878d", null ],
    [ "PPL_POLY_CON_RELATION_IS_INCLUDED", "group__Datatypes.html#ga841003bef28b1052aa2b5297a529d4d8", null ],
    [ "PPL_POLY_CON_RELATION_SATURATES", "group__Datatypes.html#gae311117a92fca76fc66fe3442ff6d09f", null ],
    [ "PPL_POLY_GEN_RELATION_SUBSUMES", "group__Datatypes.html#ga0aac22e2dc312e8c41418dd8d59701f8", null ]
];