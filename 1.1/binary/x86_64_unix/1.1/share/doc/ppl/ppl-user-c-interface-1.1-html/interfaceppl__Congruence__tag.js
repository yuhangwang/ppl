var interfaceppl__Congruence__tag =
[
    [ "ppl_new_Congruence", "interfaceppl__Congruence__tag.html#a6d24fa81c885803e34fadd9332287d2e", null ],
    [ "ppl_new_Congruence_zero_dim_false", "interfaceppl__Congruence__tag.html#a6871446de8be708c37b2bedbf12c9b76", null ],
    [ "ppl_new_Congruence_zero_dim_integrality", "interfaceppl__Congruence__tag.html#a08a4701b629d734ea0e11231a34514b0", null ],
    [ "ppl_new_Congruence_from_Congruence", "interfaceppl__Congruence__tag.html#ab67c8dacaa1610fe5ea9efe67263477d", null ],
    [ "ppl_assign_Congruence_from_Congruence", "interfaceppl__Congruence__tag.html#a2845e4528b7b42c6c6527daceee21312", null ],
    [ "ppl_delete_Congruence", "interfaceppl__Congruence__tag.html#a0399dc1a987546af5d0491e72a6a05e4", null ],
    [ "ppl_Congruence_space_dimension", "interfaceppl__Congruence__tag.html#a5e1c7e98053b2262c5f56c46e2f04f05", null ],
    [ "ppl_Congruence_coefficient", "interfaceppl__Congruence__tag.html#a553952be8bff48ab2a4a8b2820efc047", null ],
    [ "ppl_Congruence_inhomogeneous_term", "interfaceppl__Congruence__tag.html#ab76a7c2b32bbc5472f87b3fae17f5a0d", null ],
    [ "ppl_Congruence_modulus", "interfaceppl__Congruence__tag.html#a10d11983038c7047547f4a68f8ea2b70", null ],
    [ "ppl_Congruence_OK", "interfaceppl__Congruence__tag.html#acaca085247f8483085877e21527a9cd6", null ],
    [ "ppl_io_print_Congruence", "interfaceppl__Congruence__tag.html#af61686ed9ede21b05016ee29f9de882b", null ],
    [ "ppl_io_fprint_Congruence", "interfaceppl__Congruence__tag.html#a4ea3e6a8b8e929e318a5c1c450667d10", null ],
    [ "ppl_io_asprint_Congruence", "interfaceppl__Congruence__tag.html#add70a09fb9b3cb0b8b7ce79d8d9ec14c", null ],
    [ "ppl_Congruence_ascii_dump", "interfaceppl__Congruence__tag.html#a29295feb65378f2ba04c202f4b9a1bb3", null ],
    [ "ppl_Congruence_ascii_load", "interfaceppl__Congruence__tag.html#af39f78c492db753013cbfd0177ffea60", null ]
];