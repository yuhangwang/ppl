var classparma__polyhedra__library_1_1PIP__Tree__Node =
[
    [ "as_solution", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#a8ad7732b0e38be9e8c42a93bffdedbcb", null ],
    [ "as_decision", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#a7c7056a9fa57d1dd08465dbb8a862b72", null ],
    [ "OK", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#afe64e41fa2c3ef17fb78ea3ea4caa89e", null ],
    [ "number_of_artificials", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#af248bcc6fc4a4dff80fd47032a49b33c", null ],
    [ "artificials", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#ac6acd7a5076267b606f39476b3b891e4", null ],
    [ "constraints", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#a33dde59d655ee0ce0f7b7b97c7e57f93", null ],
    [ "toString", "classparma__polyhedra__library_1_1PIP__Tree__Node.html#a59f64bef2634e80385a906bfefe2acd5", null ]
];