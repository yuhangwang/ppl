var enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width =
[
    [ "BITS_8", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a0940a9ce95b204cbc7b0ba075e3802be", null ],
    [ "BITS_16", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a26d3ec66128ed1a32ecf5905b0beae28", null ],
    [ "BITS_32", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a6a6f001254ef8c3b3eddb432ca6e9dbe", null ],
    [ "BITS_64", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a239f39386ac001c6c20a50505e8bb671", null ]
];