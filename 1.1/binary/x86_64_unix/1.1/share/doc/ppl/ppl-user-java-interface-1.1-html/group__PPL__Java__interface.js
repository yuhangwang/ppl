var group__PPL__Java__interface =
[
    [ "parma_polyhedra_library", "namespaceparma__polyhedra__library.html", null ],
    [ "Artificial_Parameter_Sequence", "classparma__polyhedra__library_1_1Artificial__Parameter__Sequence.html", [
      [ "Artificial_Parameter_Sequence", "classparma__polyhedra__library_1_1Artificial__Parameter__Sequence.html#a47b1b172706010e446aa8055c982a857", null ]
    ] ],
    [ "Bounded_Integer_Type_Overflow", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Overflow.html", [
      [ "OVERFLOW_WRAPS", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Overflow.html#a1834eabb1a60cd21454609bb6af2b70c", null ],
      [ "OVERFLOW_UNDEFINED", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Overflow.html#a20f960ad44478ccbccc6ba807c88ec88", null ]
    ] ],
    [ "Bounded_Integer_Type_Representation", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Representation.html", [
      [ "UNSIGNED", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Representation.html#a5989bf59e335673996e366dbea747064", null ]
    ] ],
    [ "Bounded_Integer_Type_Width", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html", [
      [ "BITS_8", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a0940a9ce95b204cbc7b0ba075e3802be", null ],
      [ "BITS_16", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a26d3ec66128ed1a32ecf5905b0beae28", null ],
      [ "BITS_32", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a6a6f001254ef8c3b3eddb432ca6e9dbe", null ],
      [ "BITS_64", "enumparma__polyhedra__library_1_1Bounded__Integer__Type__Width.html#a239f39386ac001c6c20a50505e8bb671", null ]
    ] ],
    [ "By_Reference< T >", "classparma__polyhedra__library_1_1By__Reference_3_01T_01_4.html", [
      [ "By_Reference", "classparma__polyhedra__library_1_1By__Reference_3_01T_01_4.html#ac0b40b5fd7fce9c2e70001fe21b04ecd", null ],
      [ "set", "classparma__polyhedra__library_1_1By__Reference_3_01T_01_4.html#a8be0080275e869b6858313f3ce58879e", null ],
      [ "get", "classparma__polyhedra__library_1_1By__Reference_3_01T_01_4.html#ad3af2e55a3f9bb1410625b2f98a371e1", null ]
    ] ],
    [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html", [
      [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html#a22380e8edd12a8121b5af6a60eb05da2", null ],
      [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html#a27e2ee6c6e6f960cc523a3366249f5f9", null ],
      [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html#a3f49d1a45b861fb12fac766004b358c5", null ],
      [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html#a1ef0c686ebed5c06b004a0799f3b1599", null ],
      [ "Coefficient", "classparma__polyhedra__library_1_1Coefficient.html#afd68df98e748af269b3588ca5504bb1d", null ],
      [ "toString", "classparma__polyhedra__library_1_1Coefficient.html#a88c2f2fc304b1e27f20f200efcde2a11", null ],
      [ "getBigInteger", "classparma__polyhedra__library_1_1Coefficient.html#aebff23eb304cef5bcd9ac11ee5d7c7ed", null ]
    ] ],
    [ "Complexity_Class", "enumparma__polyhedra__library_1_1Complexity__Class.html", [
      [ "POLYNOMIAL_COMPLEXITY", "enumparma__polyhedra__library_1_1Complexity__Class.html#a5c1b244e9422e1c29c71844a9f896aee", null ],
      [ "SIMPLEX_COMPLEXITY", "enumparma__polyhedra__library_1_1Complexity__Class.html#a00d82d93d2d0113f1ea5b615decb2849", null ]
    ] ],
    [ "Congruence", "classparma__polyhedra__library_1_1Congruence.html", [
      [ "Congruence", "classparma__polyhedra__library_1_1Congruence.html#adf1055ffa2127b453280f5491cb15ffb", null ],
      [ "left_hand_side", "classparma__polyhedra__library_1_1Congruence.html#a90cc7704ce2f0ee6c205bfa3877a7671", null ],
      [ "right_hand_side", "classparma__polyhedra__library_1_1Congruence.html#a3c4dfe2a773355c5842dbe133a7c974e", null ],
      [ "modulus", "classparma__polyhedra__library_1_1Congruence.html#a894f888629036722adcbc81268cab48a", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Congruence.html#af6656a7b76ac03bbfccba848d6a42dc2", null ],
      [ "toString", "classparma__polyhedra__library_1_1Congruence.html#af0873d79c213b1552008ea16229b58e5", null ],
      [ "mod", "classparma__polyhedra__library_1_1Congruence.html#a6f45bd1aa87e7b459ee0d93a65ff05db", null ]
    ] ],
    [ "Congruence_System", "classparma__polyhedra__library_1_1Congruence__System.html", [
      [ "Congruence_System", "classparma__polyhedra__library_1_1Congruence__System.html#ae287421ae1ceab78c861efb2803c1edb", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Congruence__System.html#a559e1395399115eb381a421c8916eb1d", null ],
      [ "toString", "classparma__polyhedra__library_1_1Congruence__System.html#a6efa775d8084cb3611b1b6b512dc2827", null ]
    ] ],
    [ "Constraint", "classparma__polyhedra__library_1_1Constraint.html", [
      [ "Constraint", "classparma__polyhedra__library_1_1Constraint.html#a2531a33c20c3cb27ea7fcf0a04339c2e", null ],
      [ "left_hand_side", "classparma__polyhedra__library_1_1Constraint.html#a9fb052eb3479e107bde4cfd95599c787", null ],
      [ "right_hand_side", "classparma__polyhedra__library_1_1Constraint.html#a5b54a0e76432b52a2259a02f31fc0b83", null ],
      [ "kind", "classparma__polyhedra__library_1_1Constraint.html#a19ca194f097ddf860d1ad80961a5eda2", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Constraint.html#abd74986494a6e574bb0499b3b7109379", null ],
      [ "toString", "classparma__polyhedra__library_1_1Constraint.html#a6062c78f7cefbc808b99f35b72fcda8a", null ]
    ] ],
    [ "Constraint_System", "classparma__polyhedra__library_1_1Constraint__System.html", [
      [ "Constraint_System", "classparma__polyhedra__library_1_1Constraint__System.html#ace9427756858d751136f1e72309b9be0", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Constraint__System.html#a67b38d56d0f11bda997b393e7b5d7c5f", null ],
      [ "toString", "classparma__polyhedra__library_1_1Constraint__System.html#a172718f103bd4533e3971e8e69f9f60c", null ]
    ] ],
    [ "Control_Parameter_Name", "enumparma__polyhedra__library_1_1Control__Parameter__Name.html", [
      [ "PRICING", "enumparma__polyhedra__library_1_1Control__Parameter__Name.html#a3d873e83cf77a02b55c7d6d6d245ed23", null ]
    ] ],
    [ "Control_Parameter_Value", "enumparma__polyhedra__library_1_1Control__Parameter__Value.html", [
      [ "PRICING_STEEPEST_EDGE_FLOAT", "enumparma__polyhedra__library_1_1Control__Parameter__Value.html#ac1791e52b930ea1fc3863866a26b1a81", null ],
      [ "PRICING_STEEPEST_EDGE_EXACT", "enumparma__polyhedra__library_1_1Control__Parameter__Value.html#ad6243026e52cac8f2b4ef458ff4b760f", null ],
      [ "PRICING_TEXTBOOK", "enumparma__polyhedra__library_1_1Control__Parameter__Value.html#a7483a53d677492ed42b591525243d243", null ]
    ] ],
    [ "Degenerate_Element", "enumparma__polyhedra__library_1_1Degenerate__Element.html", [
      [ "UNIVERSE", "enumparma__polyhedra__library_1_1Degenerate__Element.html#a9ff8417659bb4800d825f91fa8685aaf", null ]
    ] ],
    [ "Domain_Error_Exception", "classparma__polyhedra__library_1_1Domain__Error__Exception.html", [
      [ "Domain_Error_Exception", "classparma__polyhedra__library_1_1Domain__Error__Exception.html#a18ac064d1fafb42b385e6b942df648f0", null ]
    ] ],
    [ "Polyhedron", "classparma__polyhedra__library_1_1Polyhedron.html", [
      [ "space_dimension", "classparma__polyhedra__library_1_1Polyhedron.html#a812e3b442d41612ca88f8905d6cb49bd", null ],
      [ "affine_dimension", "classparma__polyhedra__library_1_1Polyhedron.html#a60ee53b704270f784fad7eab1a542bfd", null ],
      [ "constraints", "classparma__polyhedra__library_1_1Polyhedron.html#a4211b10c19024d7056b4e36e4d2b7cff", null ],
      [ "congruences", "classparma__polyhedra__library_1_1Polyhedron.html#ae17f1279a841b6efc0efa6441878c025", null ],
      [ "minimized_constraints", "classparma__polyhedra__library_1_1Polyhedron.html#afd99082f48df543223c3152ca1fe600c", null ],
      [ "minimized_congruences", "classparma__polyhedra__library_1_1Polyhedron.html#a34c73f38bc99dbcb38cc7214e1e0e82d", null ],
      [ "is_empty", "classparma__polyhedra__library_1_1Polyhedron.html#a1680eecb6021ebdb33388e32932e6ecc", null ],
      [ "is_universe", "classparma__polyhedra__library_1_1Polyhedron.html#a513cc77545233d96c437ba7e1d001557", null ],
      [ "is_bounded", "classparma__polyhedra__library_1_1Polyhedron.html#a2b74eef4fc5f2e35ba14f734f0e8e167", null ],
      [ "is_discrete", "classparma__polyhedra__library_1_1Polyhedron.html#ac2e30d8774b244ed1bd206f76e786745", null ],
      [ "is_topologically_closed", "classparma__polyhedra__library_1_1Polyhedron.html#a3527c6d35935d9860c07b31970d6dfc4", null ],
      [ "contains_integer_point", "classparma__polyhedra__library_1_1Polyhedron.html#a7ce8a057ee2d4a20b8820da2bb6dc2af", null ],
      [ "constrains", "classparma__polyhedra__library_1_1Polyhedron.html#a1217cb151cbdbbcf4391987b9e2d232c", null ],
      [ "bounds_from_above", "classparma__polyhedra__library_1_1Polyhedron.html#ab6edbecd120074f2661ed2cd06ce1cd1", null ],
      [ "bounds_from_below", "classparma__polyhedra__library_1_1Polyhedron.html#a9a6e20494d2e7c618d4c6aeab45bac30", null ],
      [ "maximize", "classparma__polyhedra__library_1_1Polyhedron.html#a945f11d66765e2f542f9c4fb654589d2", null ],
      [ "minimize", "classparma__polyhedra__library_1_1Polyhedron.html#a7e4588dc8f29c46edfb1fa34e6a9d424", null ],
      [ "maximize", "classparma__polyhedra__library_1_1Polyhedron.html#a110353a7864529b85d5465ac1bce2670", null ],
      [ "minimize", "classparma__polyhedra__library_1_1Polyhedron.html#a8f50e6dcc51327c56d99dc964961d6a2", null ],
      [ "relation_with", "classparma__polyhedra__library_1_1Polyhedron.html#a52ae035a37af93cc3115f3a45f4e032c", null ],
      [ "relation_with", "classparma__polyhedra__library_1_1Polyhedron.html#abfd8a93992bbf17101c53d68a44d9986", null ],
      [ "relation_with", "classparma__polyhedra__library_1_1Polyhedron.html#ac76c332a5b267f6b6bd749cc3b9e192e", null ],
      [ "contains", "classparma__polyhedra__library_1_1Polyhedron.html#a62737490fa8ee8763ee2ff8b54847670", null ],
      [ "strictly_contains", "classparma__polyhedra__library_1_1Polyhedron.html#a950e7082860fadc45452ffe241de4488", null ],
      [ "is_disjoint_from", "classparma__polyhedra__library_1_1Polyhedron.html#a8a9706372c9b371938f04b68516bb407", null ],
      [ "equals", "classparma__polyhedra__library_1_1Polyhedron.html#ab780403b7f3997c5f5703ea0218c2841", null ],
      [ "equals", "classparma__polyhedra__library_1_1Polyhedron.html#a5c577b51724616ef96f58ce0f2fec482", null ],
      [ "hashCode", "classparma__polyhedra__library_1_1Polyhedron.html#a1e5d8d199e0bcd20aeacd72b2fef10f6", null ],
      [ "external_memory_in_bytes", "classparma__polyhedra__library_1_1Polyhedron.html#ae32bc538fedce99e602767b1065ad599", null ],
      [ "total_memory_in_bytes", "classparma__polyhedra__library_1_1Polyhedron.html#abf4b919c3b8d7dd958aab203fcabc4b4", null ],
      [ "toString", "classparma__polyhedra__library_1_1Polyhedron.html#a284af200f2e06dcfc079a58b3d4d6a59", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Polyhedron.html#afdef7f8fe0ba899b496aaa369780dfda", null ],
      [ "OK", "classparma__polyhedra__library_1_1Polyhedron.html#a3d3179300b5bf2b41a40b5b76650f8e7", null ],
      [ "add_constraint", "classparma__polyhedra__library_1_1Polyhedron.html#a4399e57484eec3301f8b88011c53c35b", null ],
      [ "add_congruence", "classparma__polyhedra__library_1_1Polyhedron.html#a3797a521b54434e13ed2843ebd88a8c1", null ],
      [ "add_constraints", "classparma__polyhedra__library_1_1Polyhedron.html#a95c109d667deed44698d16eb25efef9d", null ],
      [ "add_congruences", "classparma__polyhedra__library_1_1Polyhedron.html#aa01e5f8c42361cc03aba32786c4f9e3d", null ],
      [ "refine_with_constraint", "classparma__polyhedra__library_1_1Polyhedron.html#acdf10439693c83932d0de9a5cf35b396", null ],
      [ "refine_with_congruence", "classparma__polyhedra__library_1_1Polyhedron.html#af04e7f91e1cfaf69dcee2df81c3d64c3", null ],
      [ "refine_with_constraints", "classparma__polyhedra__library_1_1Polyhedron.html#af26aee5944d076192d5f6a12086c28f7", null ],
      [ "refine_with_congruences", "classparma__polyhedra__library_1_1Polyhedron.html#a66510fe1afe13b5f7334057bc96efa36", null ],
      [ "intersection_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a6be74fc6a3ef7d8e1c1939536af2a943", null ],
      [ "upper_bound_assign", "classparma__polyhedra__library_1_1Polyhedron.html#acd5b7ab06d657819656175f7d6a0cd0d", null ],
      [ "difference_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a6e35f9362b9f7ed65be59ae07f1da622", null ],
      [ "time_elapse_assign", "classparma__polyhedra__library_1_1Polyhedron.html#aef8d1e4a767f5ea79384588d0e19b83c", null ],
      [ "topological_closure_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a6c51199902356b837c63915caaab7a40", null ],
      [ "simplify_using_context_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a3d6404d09696f8b693b9260d267df2c3", null ],
      [ "affine_image", "classparma__polyhedra__library_1_1Polyhedron.html#a99adfdf68b5bee7b3963a15c4183e136", null ],
      [ "affine_preimage", "classparma__polyhedra__library_1_1Polyhedron.html#a501e48144147e2563edc52dcd49503f9", null ],
      [ "bounded_affine_image", "classparma__polyhedra__library_1_1Polyhedron.html#a701a35ba2b5418869a471932535cf9e5", null ],
      [ "bounded_affine_preimage", "classparma__polyhedra__library_1_1Polyhedron.html#a1b75c266a974976557206ce451e45967", null ],
      [ "generalized_affine_image", "classparma__polyhedra__library_1_1Polyhedron.html#a5955d6b18f4135b382ca0db2479c11ac", null ],
      [ "generalized_affine_preimage", "classparma__polyhedra__library_1_1Polyhedron.html#ae899d1e624241d1328d6c6421f5485e6", null ],
      [ "generalized_affine_image", "classparma__polyhedra__library_1_1Polyhedron.html#a920814c13d635e9a503551b0988c2958", null ],
      [ "generalized_affine_preimage", "classparma__polyhedra__library_1_1Polyhedron.html#a18afe96aac90a2f8f635fb0e3722fcc3", null ],
      [ "unconstrain_space_dimension", "classparma__polyhedra__library_1_1Polyhedron.html#a7f35fe17c4c48b15ddb66e52e110c2bd", null ],
      [ "unconstrain_space_dimensions", "classparma__polyhedra__library_1_1Polyhedron.html#ada449a9c1a600893585256666d6fe331", null ],
      [ "widening_assign", "classparma__polyhedra__library_1_1Polyhedron.html#ac034ebc28209bd38f90ba23323f65c41", null ],
      [ "swap", "classparma__polyhedra__library_1_1Polyhedron.html#aaec18d4d5bd29cd38e153b4602e027da", null ],
      [ "add_space_dimensions_and_embed", "classparma__polyhedra__library_1_1Polyhedron.html#a0e578573084aafbf76c85db7dd1363be", null ],
      [ "add_space_dimensions_and_project", "classparma__polyhedra__library_1_1Polyhedron.html#a3cdef8e8e717e8aae495db38ab2f3555", null ],
      [ "concatenate_assign", "classparma__polyhedra__library_1_1Polyhedron.html#ab11bfde2a1ce71a607644a38cb914b13", null ],
      [ "remove_space_dimensions", "classparma__polyhedra__library_1_1Polyhedron.html#a8561bc8f545197343c8cf1935e794af3", null ],
      [ "remove_higher_space_dimensions", "classparma__polyhedra__library_1_1Polyhedron.html#a86d72df251b72aa131ecdad46fe9a860", null ],
      [ "expand_space_dimension", "classparma__polyhedra__library_1_1Polyhedron.html#afba20cade6fa4fcd26b81086a02923e5", null ],
      [ "fold_space_dimensions", "classparma__polyhedra__library_1_1Polyhedron.html#a427ee580c9a357d0e154cd2520dbe33a", null ],
      [ "map_space_dimensions", "classparma__polyhedra__library_1_1Polyhedron.html#a00413902e11b669bca9a8c81438202ee", null ],
      [ "generators", "classparma__polyhedra__library_1_1Polyhedron.html#a9dd3a06d94e54ca59e0c41901d9f9a06", null ],
      [ "minimized_generators", "classparma__polyhedra__library_1_1Polyhedron.html#ab8ef27e25132c083ae1b59d3162fa21a", null ],
      [ "add_generator", "classparma__polyhedra__library_1_1Polyhedron.html#abdf75082ed3ae874de8fa5cb0198e080", null ],
      [ "add_generators", "classparma__polyhedra__library_1_1Polyhedron.html#a73a395dac9898b73e0782a47f4537cd7", null ],
      [ "poly_hull_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a56e3a5f9a94fb76a527fedea1bbd83cd", null ],
      [ "poly_difference_assign", "classparma__polyhedra__library_1_1Polyhedron.html#ad38a887658485f32f589a57a74e6a78c", null ],
      [ "BHRZ03_widening_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a81715f656d39c399cf4d63b72635194c", null ],
      [ "H79_widening_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a268b22472143daa3e8f2d0828d1ad565", null ],
      [ "limited_BHRZ03_extrapolation_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a9f1fcbebed65fafcc7439ce24392b59a", null ],
      [ "limited_H79_extrapolation_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a8bf0912e9b611d41ee76da66fd1bd28f", null ],
      [ "bounded_BHRZ03_extrapolation_assign", "classparma__polyhedra__library_1_1Polyhedron.html#aee40105531fe1008562584274189cf35", null ],
      [ "bounded_H79_extrapolation_assign", "classparma__polyhedra__library_1_1Polyhedron.html#a58869dfc76ca4a5a51e1e130096cfc88", null ]
    ] ],
    [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html", [
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#ae6b00d1beeeae893cca2817406d0039a", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#ad4fd0a018fd96e10fddd740337f60ebd", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#aba7be77030b5b01a9e1049ce41bbfa77", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#a5431caa9e7f5c824e9a6ac2e6d69fd87", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#a0a28bc5d3066a6ffe552a90aa688fe46", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#a6eb50dda90cfae549d191f5124f2562d", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#a922faaab6e2cb023231b56b049bca0f4", null ],
      [ "C_Polyhedron", "classparma__polyhedra__library_1_1C__Polyhedron.html#a848091b71fff6f0085950e64b6479e4a", null ],
      [ "free", "classparma__polyhedra__library_1_1C__Polyhedron.html#a4980562fb3e164d31f6a0ce66b638ce3", null ],
      [ "upper_bound_assign_if_exact", "classparma__polyhedra__library_1_1C__Polyhedron.html#af495c3499ed2742d6b68bc6bc99224d7", null ],
      [ "finalize", "classparma__polyhedra__library_1_1C__Polyhedron.html#a2cd3747157a618a88e2c95bc45451f12", null ]
    ] ],
    [ "Pointset_Powerset_C_Polyhedron", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html", [
      [ "omega_reduce", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#adf86846ee255deb41ca850ac64c4b61c", null ],
      [ "size", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a711a61f2016a1a60c7cce6dceb2bb94b", null ],
      [ "geometrically_covers", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#af3095a51638dbd4f5f64ef0abae85829", null ],
      [ "geometrically_equals", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a4ba9255b9f22cd0731c631579a578efd", null ],
      [ "begin_iterator", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a49b8e7f8a6b1ed7aaa6e91729323de7e", null ],
      [ "end_iterator", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a3ae906e1904f2ac295800c2da5ad0eb7", null ],
      [ "add_disjunct", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#afc1ff8d6faebe04f1639123b5a35e1dd", null ],
      [ "drop_disjunct", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#ad8b71c1b523782a16d5ced5b3103bd8c", null ],
      [ "drop_disjuncts", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a8adda024ba9ada688f17592347ffa4a9", null ],
      [ "pairwise_reduce", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron.html#a20fc9fa06d5e6772b60c0d8c968716a5", null ]
    ] ],
    [ "Pointset_Powerset_C_Polyhedron_Iterator", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html", [
      [ "Pointset_Powerset_C_Polyhedron_Iterator", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#acd3100605be3e85bc68685085a2572ae", null ],
      [ "equals", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a0bcdabb3d2a28f20e10697676f42e490", null ],
      [ "next", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a26b1ddc4ece928d4301c1fca3d9c2db7", null ],
      [ "prev", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a22326363cf04596cec471dbe66e03653", null ],
      [ "get_disjunct", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a1b586c1f7e68ef2ad7a47bb83767a32f", null ],
      [ "free", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a96c42895c3599ed91e187ea39f94672c", null ],
      [ "finalize", "classparma__polyhedra__library_1_1Pointset__Powerset__C__Polyhedron__Iterator.html#a12e689efd1655b4ec60adc26beb16a65", null ]
    ] ],
    [ "Generator", "classparma__polyhedra__library_1_1Generator.html", [
      [ "type", "classparma__polyhedra__library_1_1Generator.html#a7223763131fbe9f93528193576a7975c", null ],
      [ "linear_expression", "classparma__polyhedra__library_1_1Generator.html#a6c93053ad6b6bbcaa9aee01b646bba06", null ],
      [ "divisor", "classparma__polyhedra__library_1_1Generator.html#a3c40a54218985599831d143c5bb5972f", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Generator.html#a67b795731a54c5c3b470d44e10806ac5", null ],
      [ "toString", "classparma__polyhedra__library_1_1Generator.html#ae1acaaf59811107a0571b1f65a41b012", null ]
    ] ],
    [ "Generator_System", "classparma__polyhedra__library_1_1Generator__System.html", [
      [ "Generator_System", "classparma__polyhedra__library_1_1Generator__System.html#ab8be81becce1bc803566f5b19e366799", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Generator__System.html#a62ca7c2995414638d139847539631598", null ],
      [ "toString", "classparma__polyhedra__library_1_1Generator__System.html#a379e7fec2e73df8beb7904b85d7645df", null ]
    ] ],
    [ "Generator_Type", "enumparma__polyhedra__library_1_1Generator__Type.html", [
      [ "LINE", "enumparma__polyhedra__library_1_1Generator__Type.html#a3b9f75c49b0c6f00923b8f7d827ca9ad", null ],
      [ "RAY", "enumparma__polyhedra__library_1_1Generator__Type.html#aa8aee01f50041535fba84ef399e5ee81", null ],
      [ "POINT", "enumparma__polyhedra__library_1_1Generator__Type.html#a2c6506f69be68e287d31aa3db68d664b", null ]
    ] ],
    [ "Grid_Generator", "classparma__polyhedra__library_1_1Grid__Generator.html", [
      [ "type", "classparma__polyhedra__library_1_1Grid__Generator.html#a58e50b5ebd0b0f5b4d5e44690a939e34", null ],
      [ "linear_expression", "classparma__polyhedra__library_1_1Grid__Generator.html#ab557b1ec4f75e98b5b4155b932d7a6af", null ],
      [ "divisor", "classparma__polyhedra__library_1_1Grid__Generator.html#a147ad6db2e9d13bee7450666cad83d7d", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Grid__Generator.html#afb38d7eabfa00c8d068e8cdd4820d46c", null ],
      [ "toString", "classparma__polyhedra__library_1_1Grid__Generator.html#a7440d17db8648a633c9b77c512bf8f9b", null ]
    ] ],
    [ "Grid_Generator_System", "classparma__polyhedra__library_1_1Grid__Generator__System.html", [
      [ "Grid_Generator_System", "classparma__polyhedra__library_1_1Grid__Generator__System.html#afb407c2937347e7381fed31b31a46e98", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Grid__Generator__System.html#a8f448359523682c0623804af48cc2dff", null ],
      [ "toString", "classparma__polyhedra__library_1_1Grid__Generator__System.html#a4c7c1b1005912277576320fd650d1e89", null ]
    ] ],
    [ "Grid_Generator_Type", "enumparma__polyhedra__library_1_1Grid__Generator__Type.html", [
      [ "LINE", "enumparma__polyhedra__library_1_1Grid__Generator__Type.html#a02ca1ef23d4c2b965198ea42d9d683a4", null ],
      [ "PARAMETER", "enumparma__polyhedra__library_1_1Grid__Generator__Type.html#a1a63d52277531ad7883e6679a90fdc67", null ]
    ] ],
    [ "Invalid_Argument_Exception", "classparma__polyhedra__library_1_1Invalid__Argument__Exception.html", [
      [ "Invalid_Argument_Exception", "classparma__polyhedra__library_1_1Invalid__Argument__Exception.html#aefc6c6941cd663b4715cc71e446d4fa0", null ]
    ] ],
    [ "IO", "classparma__polyhedra__library_1_1IO.html", null ],
    [ "Length_Error_Exception", "classparma__polyhedra__library_1_1Length__Error__Exception.html", [
      [ "Length_Error_Exception", "classparma__polyhedra__library_1_1Length__Error__Exception.html#a84966c712051f47e1dcee5490d5ca670", null ]
    ] ],
    [ "Linear_Expression", "classparma__polyhedra__library_1_1Linear__Expression.html", [
      [ "sum", "classparma__polyhedra__library_1_1Linear__Expression.html#a0c6bf4c112511ed41878554ca3c02977", null ],
      [ "subtract", "classparma__polyhedra__library_1_1Linear__Expression.html#aa4c34f9b1017a3b44afaa423b832722a", null ],
      [ "times", "classparma__polyhedra__library_1_1Linear__Expression.html#a2cbe4b9477e2eed6411787d14c306af5", null ],
      [ "unary_minus", "classparma__polyhedra__library_1_1Linear__Expression.html#aedb7a64c3a6bdff1793482110849e5d9", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression.html#ac622b43c176dfe1b92260b841192b305", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1Linear__Expression.html#a3a962a357f4260b224518672b80ccdc6", null ],
      [ "toString", "classparma__polyhedra__library_1_1Linear__Expression.html#af6431ecb966a6e38dc55e7f4274b31db", null ],
      [ "is_zero", "classparma__polyhedra__library_1_1Linear__Expression.html#ab9e90a38fb859d1dbe667efec83743d0", null ],
      [ "all_homogeneous_terms_are_zero", "classparma__polyhedra__library_1_1Linear__Expression.html#ad7fba193bb811d3dd21903c3fb5e2428", null ]
    ] ],
    [ "Linear_Expression_Coefficient", "classparma__polyhedra__library_1_1Linear__Expression__Coefficient.html", [
      [ "Linear_Expression_Coefficient", "classparma__polyhedra__library_1_1Linear__Expression__Coefficient.html#ae2a6394e34e787c14484f3d411e022ff", null ],
      [ "argument", "classparma__polyhedra__library_1_1Linear__Expression__Coefficient.html#ad6ca74b77c4e7c632ddfdc6ab8f3567a", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Coefficient.html#a1109f21e2d8d35f3dae8ddc8074dd7cc", null ],
      [ "coeff", "classparma__polyhedra__library_1_1Linear__Expression__Coefficient.html#a733941c541eb74a6c925db946b39e46e", null ]
    ] ],
    [ "Linear_Expression_Difference", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html", [
      [ "Linear_Expression_Difference", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#af36458b7b549f4494a2554972244867b", null ],
      [ "left_hand_side", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#aba4e5801ccc48c5aca66c1e09ddb601e", null ],
      [ "right_hand_side", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#aaaeb914f871ccb4e97fc4fd513364b94", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#a2a702d0e6013ee746e60e4b3c53abfb9", null ],
      [ "lhs", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#aa575e8597c316031b6de799f6b5e55bf", null ],
      [ "rhs", "classparma__polyhedra__library_1_1Linear__Expression__Difference.html#a2e6d73507de036e41aa8e1cfc717835e", null ]
    ] ],
    [ "Linear_Expression_Sum", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html", [
      [ "Linear_Expression_Sum", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#a6d0d4f5a1f45dd0c5ccb2abe003bb041", null ],
      [ "left_hand_side", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#a75fb2490197b55a1474aff4ce0022598", null ],
      [ "right_hand_side", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#acd2297a09c09cc44bf66eaafb90e0d7c", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#a1af748b5b83815f826301969cee51712", null ],
      [ "lhs", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#a04d7bcdd3d8cd086fa28922403cbeb6e", null ],
      [ "rhs", "classparma__polyhedra__library_1_1Linear__Expression__Sum.html#aaf17ce2383d125a60b8283addae73c7c", null ]
    ] ],
    [ "Linear_Expression_Times", "classparma__polyhedra__library_1_1Linear__Expression__Times.html", [
      [ "Linear_Expression_Times", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#a84e0592d7a7e218f4c4c782f882f18cc", null ],
      [ "Linear_Expression_Times", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#ac3d22627243890fd03c69a0550485a68", null ],
      [ "Linear_Expression_Times", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#a8bddb181c1be9d2553b18871ab85e84f", null ],
      [ "coefficient", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#a2a5cc6cf6acdd7c5695af1ef807c40b2", null ],
      [ "linear_expression", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#aa37c50d4e586466d66070a61595399ec", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#aca857d6f0e8cef1c2a2b077cd6a513ef", null ],
      [ "coeff", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#abef3573de0c962f79637f379b089b0ad", null ],
      [ "lin_expr", "classparma__polyhedra__library_1_1Linear__Expression__Times.html#a0e3334c609b7d72493ee150349098048", null ]
    ] ],
    [ "Linear_Expression_Unary_Minus", "classparma__polyhedra__library_1_1Linear__Expression__Unary__Minus.html", [
      [ "Linear_Expression_Unary_Minus", "classparma__polyhedra__library_1_1Linear__Expression__Unary__Minus.html#a0b2d532ef24c4ccd4a4a04907d03f2af", null ],
      [ "argument", "classparma__polyhedra__library_1_1Linear__Expression__Unary__Minus.html#a0e57cbf0841e9af58ac2a52e89865a86", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Unary__Minus.html#adb175a61cbcdf46716687740a28e92db", null ],
      [ "arg", "classparma__polyhedra__library_1_1Linear__Expression__Unary__Minus.html#a48032da1e7c874083e027a065499bd5b", null ]
    ] ],
    [ "Linear_Expression_Variable", "classparma__polyhedra__library_1_1Linear__Expression__Variable.html", [
      [ "Linear_Expression_Variable", "classparma__polyhedra__library_1_1Linear__Expression__Variable.html#a820ba8207751c8764ab6a51eb1183a7c", null ],
      [ "argument", "classparma__polyhedra__library_1_1Linear__Expression__Variable.html#ad6f7c3aef257204c507d0a85539aa987", null ],
      [ "clone", "classparma__polyhedra__library_1_1Linear__Expression__Variable.html#a5bb6231ba1b0357cce472468fc4733f2", null ]
    ] ],
    [ "Logic_Error_Exception", "classparma__polyhedra__library_1_1Logic__Error__Exception.html", [
      [ "Logic_Error_Exception", "classparma__polyhedra__library_1_1Logic__Error__Exception.html#a655c21143c2eb90c64487e6e4dcd140c", null ]
    ] ],
    [ "MIP_Problem", "classparma__polyhedra__library_1_1MIP__Problem.html", [
      [ "MIP_Problem", "classparma__polyhedra__library_1_1MIP__Problem.html#a36ac8f2a9f570db98e652d333541ae5c", null ],
      [ "MIP_Problem", "classparma__polyhedra__library_1_1MIP__Problem.html#a8e5ebed0862d7349443d391cb8aa9ab3", null ],
      [ "MIP_Problem", "classparma__polyhedra__library_1_1MIP__Problem.html#a42d572a34bcfa6f75a730215898de8c3", null ],
      [ "free", "classparma__polyhedra__library_1_1MIP__Problem.html#a029ae33c5a3d2e301e08d77744d6b739", null ],
      [ "finalize", "classparma__polyhedra__library_1_1MIP__Problem.html#a76aa40d264daa4af312ae293615ba3d6", null ],
      [ "max_space_dimension", "classparma__polyhedra__library_1_1MIP__Problem.html#a1a9c175a241fd2ea35c88ba74074e695", null ],
      [ "space_dimension", "classparma__polyhedra__library_1_1MIP__Problem.html#a259d72e4c1cc8fbf723b463475882c9e", null ],
      [ "integer_space_dimensions", "classparma__polyhedra__library_1_1MIP__Problem.html#a8eb4ecbc8176c3018d1d742f5abb8115", null ],
      [ "constraints", "classparma__polyhedra__library_1_1MIP__Problem.html#a10856aafcb2ba1f52e5158a96950d32c", null ],
      [ "objective_function", "classparma__polyhedra__library_1_1MIP__Problem.html#a163804d4b5d0f7361974963ef22f98ee", null ],
      [ "optimization_mode", "classparma__polyhedra__library_1_1MIP__Problem.html#abc1363206f93e6778c1e9d136f24c01b", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1MIP__Problem.html#aed0b5ff233c2092ec5414bcf36747dd5", null ],
      [ "toString", "classparma__polyhedra__library_1_1MIP__Problem.html#a4b15258348ad1f31d1857eca9dd9bc68", null ],
      [ "total_memory_in_bytes", "classparma__polyhedra__library_1_1MIP__Problem.html#af95bf704d29d8b91d248d1d437ad703a", null ],
      [ "OK", "classparma__polyhedra__library_1_1MIP__Problem.html#a4155cebeafe8bc5e3d7d863e1a24abcb", null ],
      [ "clear", "classparma__polyhedra__library_1_1MIP__Problem.html#ae1928da791c8514b7e82e2d750ebdad8", null ],
      [ "add_space_dimensions_and_embed", "classparma__polyhedra__library_1_1MIP__Problem.html#a77b86c1af3be2ba98179fe26fc5560de", null ],
      [ "add_to_integer_space_dimensions", "classparma__polyhedra__library_1_1MIP__Problem.html#afbd6f23017c162cd460d4b0ac42ae4c8", null ],
      [ "add_constraint", "classparma__polyhedra__library_1_1MIP__Problem.html#a68cd1f4ad51eff8dcc8634ae7990b3e8", null ],
      [ "add_constraints", "classparma__polyhedra__library_1_1MIP__Problem.html#a81aa24851effbe4fade96c65e7c575d4", null ],
      [ "set_objective_function", "classparma__polyhedra__library_1_1MIP__Problem.html#a9971c2e55bbf3477c321cb1fed28c430", null ],
      [ "set_optimization_mode", "classparma__polyhedra__library_1_1MIP__Problem.html#a6cace6b1265203d0fff22021fd2a56a2", null ],
      [ "is_satisfiable", "classparma__polyhedra__library_1_1MIP__Problem.html#a0a9d6f3a0eb496081b86efbb0ea0a011", null ],
      [ "solve", "classparma__polyhedra__library_1_1MIP__Problem.html#af933208cb9b9013073316c1107b3649d", null ],
      [ "evaluate_objective_function", "classparma__polyhedra__library_1_1MIP__Problem.html#a535529376fbabac6dde098be2c0bc9d8", null ],
      [ "feasible_point", "classparma__polyhedra__library_1_1MIP__Problem.html#a8c602e835a5fe866cd3d0838c42ac9e3", null ],
      [ "optimizing_point", "classparma__polyhedra__library_1_1MIP__Problem.html#a3659575ce6ee121e2a2df2c347796fce", null ],
      [ "optimal_value", "classparma__polyhedra__library_1_1MIP__Problem.html#af44c1d2698cf0080c2e7cdc4fc2b9634", null ],
      [ "get_control_parameter", "classparma__polyhedra__library_1_1MIP__Problem.html#a1dc4d783858f3eff8674c04502f4432f", null ],
      [ "set_control_parameter", "classparma__polyhedra__library_1_1MIP__Problem.html#ae65e48247a4f2477e8c00f5088190bfc", null ]
    ] ],
    [ "MIP_Problem_Status", "enumparma__polyhedra__library_1_1MIP__Problem__Status.html", [
      [ "UNFEASIBLE_MIP_PROBLEM", "enumparma__polyhedra__library_1_1MIP__Problem__Status.html#a417470c5917175d267ea3ec71df18027", null ],
      [ "UNBOUNDED_MIP_PROBLEM", "enumparma__polyhedra__library_1_1MIP__Problem__Status.html#af2bdcebf7178739216e2dc7f5c421c9c", null ]
    ] ],
    [ "Optimization_Mode", "enumparma__polyhedra__library_1_1Optimization__Mode.html", [
      [ "MINIMIZATION", "enumparma__polyhedra__library_1_1Optimization__Mode.html#aed6d879ab6fa6e6dea28b6841058c848", null ]
    ] ],
    [ "Overflow_Error_Exception", "classparma__polyhedra__library_1_1Overflow__Error__Exception.html", [
      [ "Overflow_Error_Exception", "classparma__polyhedra__library_1_1Overflow__Error__Exception.html#a3c4ed48495c48bc67b46023bb0b62828", null ]
    ] ],
    [ "Pair< K, V >", "classparma__polyhedra__library_1_1Pair_3_01K_00_01V_01_4.html", [
      [ "getFirst", "classparma__polyhedra__library_1_1Pair_3_01K_00_01V_01_4.html#a0f022d23c55748a9e437e1625149bf5a", null ],
      [ "getSecond", "classparma__polyhedra__library_1_1Pair_3_01K_00_01V_01_4.html#a35e4df2723c110fbdadbcde7fe9cfae0", null ]
    ] ],
    [ "Parma_Polyhedra_Library", "classparma__polyhedra__library_1_1Parma__Polyhedra__Library.html", null ],
    [ "Partial_Function", "classparma__polyhedra__library_1_1Partial__Function.html", [
      [ "Partial_Function", "classparma__polyhedra__library_1_1Partial__Function.html#a940d4cc630004e148b25a81010b12d61", null ],
      [ "insert", "classparma__polyhedra__library_1_1Partial__Function.html#ab8e3183f9373557491bf9d9cee30548d", null ],
      [ "has_empty_codomain", "classparma__polyhedra__library_1_1Partial__Function.html#aa348a444933ae006cd061eb2a01af5b7", null ],
      [ "max_in_codomain", "classparma__polyhedra__library_1_1Partial__Function.html#a52e000efba75d51e7891f8e9da8e4e1b", null ],
      [ "maps", "classparma__polyhedra__library_1_1Partial__Function.html#a054857677ca99c51342e420e9d5bad9f", null ],
      [ "free", "classparma__polyhedra__library_1_1Partial__Function.html#a0861934c38d4b7e0432b6ba71a40df2d", null ],
      [ "finalize", "classparma__polyhedra__library_1_1Partial__Function.html#a661ab4cd615864ae392638259f9c58da", null ]
    ] ],
    [ "PIP_Problem", "classparma__polyhedra__library_1_1PIP__Problem.html", [
      [ "PIP_Problem", "classparma__polyhedra__library_1_1PIP__Problem.html#ad7ae9489e4e3449bfedbafd36d929f55", null ],
      [ "PIP_Problem", "classparma__polyhedra__library_1_1PIP__Problem.html#ae7dc52a107e3c90f03801ac327d35589", null ],
      [ "PIP_Problem", "classparma__polyhedra__library_1_1PIP__Problem.html#a77791a65ff33495b86466f4a672cd531", null ],
      [ "free", "classparma__polyhedra__library_1_1PIP__Problem.html#ab75311edf7727278b2c6621e4cf08986", null ],
      [ "finalize", "classparma__polyhedra__library_1_1PIP__Problem.html#a9117ebcf4233e25be986d6b67ca78f1d", null ],
      [ "max_space_dimension", "classparma__polyhedra__library_1_1PIP__Problem.html#a4c79cc34280a10e3ca9d2a2fdb8f52fa", null ],
      [ "space_dimension", "classparma__polyhedra__library_1_1PIP__Problem.html#ad67204128201f54bc7e543a16e67e951", null ],
      [ "number_of_parameter_space_dimensions", "classparma__polyhedra__library_1_1PIP__Problem.html#ae2e189679413b6058b03b13fc1b8dcfd", null ],
      [ "parameter_space_dimensions", "classparma__polyhedra__library_1_1PIP__Problem.html#ac9e60b83b647eaf62c173ecf3539c536", null ],
      [ "get_big_parameter_dimension", "classparma__polyhedra__library_1_1PIP__Problem.html#a28b832ea15d4a33b33f51554b5f54a43", null ],
      [ "number_of_constraints", "classparma__polyhedra__library_1_1PIP__Problem.html#a6adb9a394696d907389f2fabc3d7f643", null ],
      [ "constraint_at_index", "classparma__polyhedra__library_1_1PIP__Problem.html#a89335d3e61d877bf25e41e562bf2fb34", null ],
      [ "constraints", "classparma__polyhedra__library_1_1PIP__Problem.html#aa86001c9fa5a81f40582bac023132a68", null ],
      [ "ascii_dump", "classparma__polyhedra__library_1_1PIP__Problem.html#a84f5ab2a93289e1436a1ea7ef58fb460", null ],
      [ "toString", "classparma__polyhedra__library_1_1PIP__Problem.html#a8959e3a2e2612143cb56991236663058", null ],
      [ "total_memory_in_bytes", "classparma__polyhedra__library_1_1PIP__Problem.html#a01868d21a17d9549b711240123d5b81c", null ],
      [ "external_memory_in_bytes", "classparma__polyhedra__library_1_1PIP__Problem.html#a373a253b31500ca4f15cf2b22e3b9eea", null ],
      [ "OK", "classparma__polyhedra__library_1_1PIP__Problem.html#aed3b733dc78567027867440a5774e70b", null ],
      [ "clear", "classparma__polyhedra__library_1_1PIP__Problem.html#a5bc4c5bb38b21a01517358b417867797", null ],
      [ "add_space_dimensions_and_embed", "classparma__polyhedra__library_1_1PIP__Problem.html#a777f48d40962e0a1f7ddcd691bde154d", null ],
      [ "add_to_parameter_space_dimensions", "classparma__polyhedra__library_1_1PIP__Problem.html#aaacb839f41a9e7355e1e1e5cc9ad0894", null ],
      [ "set_big_parameter_dimension", "classparma__polyhedra__library_1_1PIP__Problem.html#adbae3f77deab71eb8d31dc8e551bdeed", null ],
      [ "add_constraint", "classparma__polyhedra__library_1_1PIP__Problem.html#a925ebc675d3deed289aed0d97dafa81c", null ],
      [ "add_constraints", "classparma__polyhedra__library_1_1PIP__Problem.html#a29bc2f9d0a2c4c96011fe36312a53e2d", null ],
      [ "is_satisfiable", "classparma__polyhedra__library_1_1PIP__Problem.html#a5c952c2540fc4ea96e8aaa1109d6ea0c", null ],
      [ "solve", "classparma__polyhedra__library_1_1PIP__Problem.html#ac41bfa66bbd6a2c52616ea5d4a6270a0", null ],
      [ "solution", "classparma__polyhedra__library_1_1PIP__Problem.html#a92ced4994293ff25212f86d55421a48a", null ],
      [ "optimizing_solution", "classparma__polyhedra__library_1_1PIP__Problem.html#a7a934327db411e24b7c2a073983d2a3e", null ],
      [ "get_pip_problem_control_parameter", "classparma__polyhedra__library_1_1PIP__Problem.html#a42e9887cb1bd2e0914862d448881d047", null ],
      [ "set_pip_problem_control_parameter", "classparma__polyhedra__library_1_1PIP__Problem.html#ab3b703e4e468da3a5f49ad28905527e9", null ]
    ] ],
    [ "PIP_Problem_Control_Parameter_Name", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Name.html", [
      [ "CUTTING_STRATEGY", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Name.html#a1676341382601ab9c96f035983f372d5", null ],
      [ "PIVOT_ROW_STRATEGY", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Name.html#ab5cadae2d9d1e506769cafc7dd19c5b5", null ]
    ] ],
    [ "PIP_Problem_Control_Parameter_Value", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html", [
      [ "CUTTING_STRATEGY_FIRST", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html#ac2b60fec8498b08d7f22b2264d3948d4", null ],
      [ "CUTTING_STRATEGY_DEEPEST", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html#a7d97a0adb78a51c7089586f7ee4cb6bd", null ],
      [ "CUTTING_STRATEGY_ALL", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html#abc9126a51dec07227e4919899e44589b", null ],
      [ "PIVOT_ROW_STRATEGY_FIRST", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html#a46e366594cc5fa0e10c75a1b6c17df09", null ],
      [ "PIVOT_ROW_STRATEGY_MAX_COLUMN", "enumparma__polyhedra__library_1_1PIP__Problem__Control__Parameter__Value.html#abd8921ad9d2cc32e7ee32de63704c1c8", null ]
    ] ],
    [ "PIP_Problem_Status", "enumparma__polyhedra__library_1_1PIP__Problem__Status.html", [
      [ "UNFEASIBLE_PIP_PROBLEM", "enumparma__polyhedra__library_1_1PIP__Problem__Status.html#a89f381f0a22cc6f22b445a4c1691db77", null ]
    ] ],
    [ "Poly_Con_Relation", "classparma__polyhedra__library_1_1Poly__Con__Relation.html", [
      [ "Poly_Con_Relation", "classparma__polyhedra__library_1_1Poly__Con__Relation.html#ae9b4bcc84019980d7be4ace75e54c0b3", null ],
      [ "implies", "classparma__polyhedra__library_1_1Poly__Con__Relation.html#adc93ada4ec022b8e6020c91412e0b0ef", null ]
    ] ],
    [ "Relation_Symbol", "enumparma__polyhedra__library_1_1Relation__Symbol.html", [
      [ "LESS_THAN", "enumparma__polyhedra__library_1_1Relation__Symbol.html#aaa76c2fe8629d0c3251374d87a0da4a4", null ],
      [ "LESS_OR_EQUAL", "enumparma__polyhedra__library_1_1Relation__Symbol.html#ac49c86d7db3259a07d9a2b293e573733", null ],
      [ "EQUAL", "enumparma__polyhedra__library_1_1Relation__Symbol.html#a95eb0b200e5754f1b18972012c1807d4", null ],
      [ "GREATER_OR_EQUAL", "enumparma__polyhedra__library_1_1Relation__Symbol.html#ae25095de7466faeb9f7bf4c476f34cf0", null ],
      [ "GREATER_THAN", "enumparma__polyhedra__library_1_1Relation__Symbol.html#ab6fa928be2e09cfafd92a225199aa362", null ]
    ] ],
    [ "Timeout_Exception", "classparma__polyhedra__library_1_1Timeout__Exception.html", [
      [ "Timeout_Exception", "classparma__polyhedra__library_1_1Timeout__Exception.html#a66765523c60cdb348393876561c5d0e7", null ]
    ] ],
    [ "Variable", "classparma__polyhedra__library_1_1Variable.html", [
      [ "Variable", "classparma__polyhedra__library_1_1Variable.html#a008168f710b00996c0183f79c23031fb", null ],
      [ "id", "classparma__polyhedra__library_1_1Variable.html#a9fb15acabc414c238c79e7c4f211c185", null ],
      [ "compareTo", "classparma__polyhedra__library_1_1Variable.html#ab54edfb4d01886f38228f56771aa085d", null ]
    ] ],
    [ "Variable_Stringifier", "interfaceparma__polyhedra__library_1_1Variable__Stringifier.html", null ]
];