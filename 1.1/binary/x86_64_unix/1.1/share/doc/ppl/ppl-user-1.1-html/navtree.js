var NAVTREE =
[
  [ "PPL", "index.html", [
    [ "General Information on the PPL", "index.html", [
      [ "The Main Features", "index.html#preamble", [
        [ "Semantic Geometric Descriptors", "index.html#Semantic_Geometric_Descriptors", null ],
        [ "Syntactic Geometric Descriptors", "index.html#Syntactic_Geometric_Descriptors", [
          [ "Basic Geometric Descriptors", "index.html#Basic_Geometric_Descriptors", null ],
          [ "Constraint Geometric Descriptors", "index.html#Constraint_Geometric_Descriptors", null ],
          [ "Generator Geometric Descriptors", "index.html#Generator_Geometric_Descriptors", null ]
        ] ],
        [ "Generic Operations on Semantic Geometric Descriptors", "index.html#Generic_Operations_on_Semantic_Geometric_Descriptors", null ]
      ] ],
      [ "Upward Approximation", "index.html#Upward_Approximation", null ],
      [ "Approximating Integers", "index.html#Approximating_Integers", [
        [ "Dropping Non-Integer Points", "index.html#Dropping_Non_Integer_Points", null ],
        [ "Approximating Bounded Integers", "index.html#Approximating_Bounded_Integers", [
          [ "Wrapping Operator", "index.html#Wrapping_Operator", null ]
        ] ]
      ] ],
      [ "Convex Polyhedra", "index.html#convex_polys", [
        [ "Vectors, Matrices and Scalar Products", "index.html#Vectors_Matrices_and_Scalar_Products", null ],
        [ "Affine Hyperplanes and Half-spaces", "index.html#Affine_Hyperplanes_and_Half_spaces", null ],
        [ "Convex Polyhedra", "index.html#Convex_Polyhedra", null ],
        [ "Bounded Polyhedra", "index.html#Bounded_Polyhedra", null ]
      ] ],
      [ "Representations of Convex Polyhedra", "index.html#representation", [
        [ "Constraints Representation", "index.html#Constraints_Representation", null ],
        [ "Combinations and Hulls", "index.html#Combinations_and_Hulls", null ],
        [ "Points, Closure Points, Rays and Lines", "index.html#Points_Closure_Points_Rays_and_Lines", null ],
        [ "Generators Representation", "index.html#Generators_Representation", null ],
        [ "Minimized Representations", "index.html#Minimized_Representations", null ],
        [ "Double Description", "index.html#Double_Description", null ],
        [ "Topologies and Topological-compatibility", "index.html#Topologies_and_Topological_compatibility", null ],
        [ "Space Dimensions and Dimension Compatibility", "index.html#Space_Dimensions_and_Dimension_Compatibility", null ],
        [ "Affine Independence and Affine Dimension", "index.html#Affine_Independence_and_Affine_Dimension", null ],
        [ "Rational Polyhedra", "index.html#Rational_Polyhedra", null ]
      ] ],
      [ "Operations on Convex Polyhedra", "index.html#Operations_on_Convex_Polyhedra", [
        [ "Intersection and Convex Polyhedral Hull", "index.html#Intersection_and_Convex_Polyhedral_Hull", null ],
        [ "Convex Polyhedral Difference", "index.html#Convex_Polyhedral_Difference", null ],
        [ "Concatenating Polyhedra", "index.html#Concatenating_Polyhedra", null ],
        [ "Adding New Dimensions to the Vector Space", "index.html#Adding_New_Dimensions_to_the_Vector_Space", null ],
        [ "Removing Dimensions from the Vector Space", "index.html#Removing_Dimensions_from_the_Vector_Space", null ],
        [ "Mapping the Dimensions of the Vector Space", "index.html#Mapping_the_Dimensions_of_the_Vector_Space", null ],
        [ "Expanding One Dimension of the Vector Space to Multiple Dimensions", "index.html#Expanding_One_Dimension_of_the_Vector_Space_to_Multiple_Dimensions", null ],
        [ "Folding Multiple Dimensions of the Vector Space into One Dimension", "index.html#Folding_Multiple_Dimensions_of_the_Vector_Space_into_One_Dimension", null ],
        [ "Images and Preimages of Affine Transfer Relations", "index.html#Images_and_Preimages_of_Affine_Transfer_Relations", null ],
        [ "Single-Update Affine Functions.", "index.html#Single_Update_Affine_Functions", null ],
        [ "Single-Update Bounded Affine Relations.", "index.html#Single_Update_Bounded_Affine_Relations", null ],
        [ "Affine Form Relations.", "index.html#affine_form_relation", null ],
        [ "Generalized Affine Relations.", "index.html#Generalized_Affine_Relations", null ],
        [ "Cylindrification Operator", "index.html#Cylindrification", null ],
        [ "Time-Elapse Operator", "index.html#Time_Elapse_Operator", null ],
        [ "Positive Time-Elapse Operator", "index.html#Positive_Time_Elapse_Operator", null ],
        [ "Meet-Preserving Enlargement and Simplification", "index.html#Meet_Preserving_Simplification", null ],
        [ "Relation-With Operators", "index.html#Relation_With_Operators", null ],
        [ "Widening Operators", "index.html#Widening_Operators", null ],
        [ "Widening with Tokens", "index.html#Widening_with_Tokens", null ],
        [ "Extrapolation Operators", "index.html#Extrapolation_Operators", null ]
      ] ],
      [ "Intervals and Boxes", "index.html#Intervals_and_Boxes", [
        [ "Widening and Extrapolation Operators on Boxes", "index.html#Widening_and_Extrapolation_Operators_on_Boxes", null ]
      ] ],
      [ "Weakly-Relational Shapes", "index.html#Weakly_Relational_Shapes", [
        [ "Bounded Difference Shapes", "index.html#Bounded_Difference_Shapes", null ],
        [ "Octagonal Shapes", "index.html#Octagonal_Shapes", null ],
        [ "Weakly-Relational Shapes Interface", "index.html#Weakly_Relational_Shape_Interface", null ],
        [ "Widening and Extrapolation Operators on Weakly-Relational Shapes", "index.html#Widening_and_Extrapolation_Operators_on_WR_Shapes", null ]
      ] ],
      [ "Rational Grids", "index.html#sect_rational_grids", [
        [ "Congruences and Congruence Relations", "index.html#Congruence_Relations", null ],
        [ "Rational Grids", "index.html#Rational_Grids", null ],
        [ "Integer Combinations", "index.html#Integer_Combinations", null ],
        [ "Points, Parameters and Lines", "index.html#Points_Parameters_Lines", null ],
        [ "The Grid Generator Representation", "index.html#Grid_Generator_Representation", null ],
        [ "Minimized Grid Representations", "index.html#Grid_Minimized_Representations", null ],
        [ "Double Description for Grids", "index.html#Grids_Double_Description_Grids", null ],
        [ "Space Dimensions and Dimension-compatibility for Grids", "index.html#Grid_Space_Dimensions", null ],
        [ "Affine Independence and Affine Dimension for Grids", "index.html#Grid_Affine_Dimension", null ]
      ] ],
      [ "Operations on Rational Grids", "index.html#rational_grid_operations", [
        [ "Affine Images and Preimages", "index.html#Grid_Affine_Transformation", null ],
        [ "Generalized Affine Images", "index.html#Grid_Generalized_Image", null ],
        [ "Frequency Operator", "index.html#Grid_Frequency", null ],
        [ "Time-Elapse Operator", "index.html#Grid_Time_Elapse", null ],
        [ "Relation-with Operators", "index.html#Grid_Relation_With", null ],
        [ "Wrapping Operator", "index.html#Grid_Wrapping_Operator", null ],
        [ "Widening Operators", "index.html#Grid_Widening", null ],
        [ "Widening with Tokens", "index.html#Grid_Widening_with_Tokens", null ],
        [ "Extrapolation Operators", "index.html#Grid_Extrapolation", null ]
      ] ],
      [ "The Powerset Construction", "index.html#powerset", [
        [ "The Powerset Domain", "index.html#The_Powerset_Domain", null ]
      ] ],
      [ "Operations on the Powerset Construction", "index.html#ps_operations", [
        [ "Meet and Upper Bound", "index.html#Meet_and_Upper_Bound", null ],
        [ "Adding a Disjunct", "index.html#Adding_a_Disjunct", null ],
        [ "Collapsing a Powerset Element", "index.html#Collapsing_a_Powerset_Element", null ]
      ] ],
      [ "The Pointset Powerset Domain", "index.html#pointset_powerset", [
        [ "Meet-Preserving Simplification", "index.html#Powerset_Meet_Preserving_Simplification", null ],
        [ "Geometric Comparisons", "index.html#Geometric_Comparisons", null ],
        [ "Pairwise Merge", "index.html#Pairwise_Merge", null ],
        [ "Powerset Extrapolation Operators", "index.html#Powerset_Extrapolation_Operators", null ],
        [ "Certificate-Based Widenings", "index.html#Certificate_Based_Widenings", null ]
      ] ],
      [ "Analysis of floating point computations", "index.html#floating_point", [
        [ "Linear forms with interval coefficients", "index.html#interval_linear_forms", null ],
        [ "Use of other abstract domains for floating point analysis", "index.html#fp_abstract_domains", null ]
      ] ],
      [ "Using the Library", "index.html#use_of_library", [
        [ "A Note on the Implementation of the Operators", "index.html#A_Note_on_the_Implementation_of_the_Operators", null ],
        [ "On Pointset_Powerset and Partially_Reduced_Product Domains: A Warning", "index.html#On_Pointset_Powerset_and_Partially_Reduced_Product_Domains_A_Warning", null ],
        [ "On Object-Orientation and Polymorphism: A Disclaimer", "index.html#On_Object_Orientation_and_Polymorphism_A_Disclaimer", null ],
        [ "On Const-Correctness: A Warning about the Use of References and Iterators", "index.html#On_Const_Correctness_A_Warning_about_the_Use_of_References_and_Iterators", null ]
      ] ],
      [ "Bibliography", "index.html#bibliography", null ]
    ] ],
    [ "GNU General Public License", "GPL.html", null ],
    [ "GNU Free Documentation License", "GFDL.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classParma__Polyhedra__Library_1_1Box.html#ad02ce2da26b1d9dcd0d45042b9f5c3e5",
"classParma__Polyhedra__Library_1_1Congruence.html#adeee840f3313b6050d439dbe5fe2fdd7",
"classParma__Polyhedra__Library_1_1Difference__Floating__Point__Expression.html#a3ae2260455d60d464a71a9b82e49cc9e",
"classParma__Polyhedra__Library_1_1Grid.html#aa26f672c32271db69ed68fc60eee8cff",
"classParma__Polyhedra__Library_1_1Interval.html#af859c3d302c3573b8e2183e4370321b0",
"classParma__Polyhedra__Library_1_1MIP__Problem_1_1const__iterator.html#a4699390a942c1aaeaccd2b89b67e5855",
"classParma__Polyhedra__Library_1_1PIP__Solution__Node.html#a49e1548e93467d9fd12ba992f8712c56",
"classParma__Polyhedra__Library_1_1Pointset__Powerset.html#acfb675b82bc6829361d1647880a2e00d",
"classParma__Polyhedra__Library_1_1Unary__Operator.html",
"index.html#Dropping_Non_Integer_Points"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var navTreeSubIndices = new Array();

function getData(varName)
{
  var i = varName.lastIndexOf('/');
  var n = i>=0 ? varName.substring(i+1) : varName;
  return eval(n.replace(/\-/g,'_'));
}

function stripPath(uri)
{
  return uri.substring(uri.lastIndexOf('/')+1);
}

function stripPath2(uri)
{
  var i = uri.lastIndexOf('/');
  var s = uri.substring(i+1);
  var m = uri.substring(0,i+1).match(/\/d\w\/d\w\w\/$/);
  return m ? uri.substring(i-6) : s;
}

function localStorageSupported()
{
  try {
    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;
  }
  catch(e) {
    return false;
  }
}


function storeLink(link)
{
  if (!$("#nav-sync").hasClass('sync') && localStorageSupported()) {
      window.localStorage.setItem('navpath',link);
  }
}

function deleteLink()
{
  if (localStorageSupported()) {
    window.localStorage.setItem('navpath','');
  } 
}

function cachedLink()
{
  if (localStorageSupported()) {
    return window.localStorage.getItem('navpath');
  } else {
    return '';
  }
}

function getScript(scriptName,func,show)
{
  var head = document.getElementsByTagName("head")[0]; 
  var script = document.createElement('script');
  script.id = scriptName;
  script.type = 'text/javascript';
  script.onload = func; 
  script.src = scriptName+'.js'; 
  if ($.browser.msie && $.browser.version<=8) { 
    // script.onload does not work with older versions of IE
    script.onreadystatechange = function() {
      if (script.readyState=='complete' || script.readyState=='loaded') { 
        func(); if (show) showRoot(); 
      }
    }
  }
  head.appendChild(script); 
}

function createIndent(o,domNode,node,level)
{
  var level=-1;
  var n = node;
  while (n.parentNode) { level++; n=n.parentNode; }
  var imgNode = document.createElement("img");
  imgNode.style.paddingLeft=(16*level).toString()+'px';
  imgNode.width  = 16;
  imgNode.height = 22;
  imgNode.border = 0;
  if (node.childrenData) {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() {
      if (node.expanded) {
        $(node.getChildrenUL()).slideUp("fast");
        node.plus_img.src = node.relpath+"ftv2pnode.png";
        node.expanded = false;
      } else {
        expandNode(o, node, false, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
    imgNode.src = node.relpath+"ftv2pnode.png";
  } else {
    imgNode.src = node.relpath+"ftv2node.png";
    domNode.appendChild(imgNode);
  } 
}

var animationInProgress = false;

function gotoAnchor(anchor,aname,updateLocation)
{
  var pos, docContent = $('#doc-content');
  if (anchor.parent().attr('class')=='memItemLeft' ||
      anchor.parent().attr('class')=='fieldtype' ||
      anchor.parent().is(':header')) 
  {
    pos = anchor.parent().position().top;
  } else if (anchor.position()) {
    pos = anchor.position().top;
  }
  if (pos) {
    var dist = Math.abs(Math.min(
               pos-docContent.offset().top,
               docContent[0].scrollHeight-
               docContent.height()-docContent.scrollTop()));
    animationInProgress=true;
    docContent.animate({
      scrollTop: pos + docContent.scrollTop() - docContent.offset().top
    },Math.max(50,Math.min(500,dist)),function(){
      if (updateLocation) window.location.href=aname;
      animationInProgress=false;
    });
  }
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  node.expanded = false;
  a.appendChild(node.label);
  if (link) {
    var url;
    if (link.substring(0,1)=='^') {
      url = link.substring(1);
      link = url;
    } else {
      url = node.relpath+link;
    }
    a.className = stripPath(link.replace('#',':'));
    if (link.indexOf('#')!=-1) {
      var aname = '#'+link.split('#')[1];
      var srcPage = stripPath($(location).attr('pathname'));
      var targetPage = stripPath(link.split('#')[0]);
      a.href = srcPage!=targetPage ? url : "javascript:void(0)"; 
      a.onclick = function(){
        storeLink(link);
        if (!$(a).parent().parent().hasClass('selected'))
        {
          $('.item').removeClass('selected');
          $('.item').removeAttr('id');
          $(a).parent().parent().addClass('selected');
          $(a).parent().parent().attr('id','selected');
        }
        var anchor = $(aname);
        gotoAnchor(anchor,aname,true);
      };
    } else {
      a.href = url;
      a.onclick = function() { storeLink(link); }
    }
  } else {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() {
    if (!node.childrenUL) {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  (function (){ // retry until we can scroll to the selected item
    try {
      var navtree=$('#nav-tree');
      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
    } catch (err) {
      setTimeout(arguments.callee, 0);
    }
  })();
}

function expandNode(o, node, imm, showRoot)
{
  if (node.childrenData && !node.expanded) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        expandNode(o, node, imm, showRoot);
      }, showRoot);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      } if (imm || ($.browser.msie && $.browser.version>8)) { 
        // somehow slideDown jumps to the start of tree for IE9 :-(
        $(node.getChildrenUL()).show();
      } else {
        $(node.getChildrenUL()).slideDown("fast");
      }
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
    }
  }
}

function glowEffect(n,duration)
{
  n.addClass('glow').delay(duration).queue(function(next){
    $(this).removeClass('glow');next();
  });
}

function highlightAnchor()
{
  var aname = $(location).attr('hash');
  var anchor = $(aname);
  if (anchor.parent().attr('class')=='memItemLeft'){
    var rows = $('.memberdecls tr[class$="'+
               window.location.hash.substring(1)+'"]');
    glowEffect(rows.children(),300); // member without details
  } else if (anchor.parents().slice(2).prop('tagName')=='TR') {
    glowEffect(anchor.parents('div.memitem'),1000); // enum value
  } else if (anchor.parent().attr('class')=='fieldtype'){
    glowEffect(anchor.parent().parent(),1000); // struct field
  } else if (anchor.parent().is(":header")) {
    glowEffect(anchor.parent(),1000); // section header
  } else {
    glowEffect(anchor.next(),1000); // normal member
  }
  gotoAnchor(anchor,aname,false);
}

function selectAndHighlight(hash,n)
{
  var a;
  if (hash) {
    var link=stripPath($(location).attr('pathname'))+':'+hash.substring(1);
    a=$('.item a[class$="'+link+'"]');
  }
  if (a && a.length) {
    a.parent().parent().addClass('selected');
    a.parent().parent().attr('id','selected');
    highlightAnchor();
  } else if (n) {
    $(n.itemDiv).addClass('selected');
    $(n.itemDiv).attr('id','selected');
  }
  if ($('#nav-tree-contents .item:first').hasClass('selected')) {
    $('#nav-sync').css('top','30px');
  } else {
    $('#nav-sync').css('top','5px');
  }
  showRoot();
}

function showNode(o, node, index, hash)
{
  if (node && node.childrenData) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        showNode(o,node,index,hash);
      },true);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      }
      $(node.getChildrenUL()).show();
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
      var n = node.children[o.breadcrumbs[index]];
      if (index+1<o.breadcrumbs.length) {
        showNode(o,n,index+1,hash);
      } else {
        if (typeof(n.childrenData)==='string') {
          var varName = n.childrenData;
          getScript(n.relpath+varName,function(){
            n.childrenData = getData(varName);
            node.expanded=false;
            showNode(o,node,index,hash); // retry with child node expanded
          },true);
        } else {
          var rootBase = stripPath(o.toroot.replace(/\..+$/, ''));
          if (rootBase=="index" || rootBase=="pages" || rootBase=="search") {
            expandNode(o, n, true, true);
          }
          selectAndHighlight(hash,n);
        }
      }
    }
  } else {
    selectAndHighlight(hash);
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
      i==l);
  }
}

function gotoNode(o,subIndex,root,hash,relpath)
{
  var nti = navTreeSubIndices[subIndex][root+hash];
  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);
  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index
    navTo(o,NAVTREE[0][1],"",relpath);
    $('.item').removeClass('selected');
    $('.item').removeAttr('id');
  }
  if (o.breadcrumbs) {
    o.breadcrumbs.unshift(0); // add 0 for root node
    showNode(o, o.node, 0, hash);
  }
}

function navTo(o,root,hash,relpath)
{
  var link = cachedLink();
  if (link) {
    var parts = link.split('#');
    root = parts[0];
    if (parts.length>1) hash = '#'+parts[1];
    else hash='';
  }
  if (hash.match(/^#l\d+$/)) {
    var anchor=$('a[name='+hash.substring(1)+']');
    glowEffect(anchor.parent(),1000); // line number
    hash=''; // strip line number anchors
    //root=root.replace(/_source\./,'.'); // source link to doc link
  }
  var url=root+hash;
  var i=-1;
  while (NAVTREEINDEX[i+1]<=url) i++;
  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index
  if (navTreeSubIndices[i]) {
    gotoNode(o,i,root,hash,relpath)
  } else {
    getScript(relpath+'navtreeindex'+i,function(){
      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);
      if (navTreeSubIndices[i]) {
        gotoNode(o,i,root,hash,relpath);
      }
    },true);
  }
}

function showSyncOff(n,relpath)
{
    n.html('<img src="'+relpath+'sync_off.png" title="'+SYNCOFFMSG+'"/>');
}

function showSyncOn(n,relpath)
{
    n.html('<img src="'+relpath+'sync_on.png" title="'+SYNCONMSG+'"/>');
}

function toggleSyncButton(relpath)
{
  var navSync = $('#nav-sync');
  if (navSync.hasClass('sync')) {
    navSync.removeClass('sync');
    showSyncOff(navSync,relpath);
    storeLink(stripPath2($(location).attr('pathname'))+$(location).attr('hash'));
  } else {
    navSync.addClass('sync');
    showSyncOn(navSync,relpath);
    deleteLink();
  }
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;
  o.node.expanded = false;
  o.node.isLast = true;
  o.node.plus_img = document.createElement("img");
  o.node.plus_img.src = relpath+"ftv2pnode.png";
  o.node.plus_img.width = 16;
  o.node.plus_img.height = 22;

  if (localStorageSupported()) {
    var navSync = $('#nav-sync');
    if (cachedLink()) {
      showSyncOff(navSync,relpath);
      navSync.removeClass('sync');
    } else {
      showSyncOn(navSync,relpath);
    }
    navSync.click(function(){ toggleSyncButton(relpath); });
  }

  navTo(o,toroot,window.location.hash,relpath);

  $(window).bind('hashchange', function(){
     if (window.location.hash && window.location.hash.length>1){
       var a;
       if ($(location).attr('hash')){
         var clslink=stripPath($(location).attr('pathname'))+':'+
                               $(location).attr('hash').substring(1);
         a=$('.item a[class$="'+clslink+'"]');
       }
       if (a==null || !$(a).parent().parent().hasClass('selected')){
         $('.item').removeClass('selected');
         $('.item').removeAttr('id');
       }
       var link=stripPath2($(location).attr('pathname'));
       navTo(o,link,$(location).attr('hash'),relpath);
     } else if (!animationInProgress) {
       $('#doc-content').scrollTop(0);
       $('.item').removeClass('selected');
       $('.item').removeAttr('id');
       navTo(o,toroot,window.location.hash,relpath);
     }
  })

  $(window).load(showRoot);
}

