# PPL
**The Parma Polyhedra Library (PPL)**

This is an unofficial verbatim redistribution of the binary&source form of the PPL library (a prerequisite for compiling programs like GCC 4.7), under the terms of GPL 3.0 license.

This redistribution is under the the same GPL 3.0 license.

Please visit the official website for more details: http://bugseng.com/products/ppl

## Usage
The binary/ folder contains both a tar.gz file and a folder, which are equivalent. You can use either one.

## Compilation notes
### Compilation environment
* CentOS 6.6
* x86_64 architecture
* Compiler: gcc version 4.4.7 20120313 (Red Hat 4.4.7-11)

### Compilation steps
```bash
wget wget ftp://ftp.cs.unipr.it/pub/ppl/releases/1.1/ppl-1.1.tar.gz
tar xvf ppl-1.1.tar.gz
mkdir build_ppl-1.1
cd build_ppl-1.1
../ppl-1.1/configure --prefix=/home/steven/install/ppl/1.1 --with-gmp=/home/steven/install/libgmp/with_cxx_support/for_gcc_4.4.7/4.3.2
make -j10
make check -j10 | tee QualityVerification.txt
make install
```

Note that PPL requires a version of GMP library to be compiled with "--enabled-cxx".

For ppl version 0.11, the installation procedure is same except for the first step should be:
```bash
wget ftp://gcc.gnu.org/pub/gcc/infrastructure/ppl-0.11.tar.gz
```

### Quality verification
See the "QualityVerification.txt" for the output of "make check".